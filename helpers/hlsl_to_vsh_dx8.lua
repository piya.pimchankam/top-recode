--[[
o-----------------------------------------------------------------------------o
| hlsl_to_vsh                                                                 |
(-----------------------------------------------------------------------------)
| By deguix                / An Utility for top-recode | Compatible w/ LuaJIT |
|                         ----------------------------------------------------|
|   Converts hlsl files to vsh files - compiles hlsl into vsh DX9/DX8 shaders.|
o-----------------------------------------------------------------------------o
--]]

--[[
	hlsl_to_vsh_folder(hlsl_folder, vsh_folder[, dx8])
	Converts whole hlsl_folder folder into vsh files put into vsh_folder.
	
	Parameters:
		hlsl_folder: path to the folder using foward slashes and no ending slash containing hlsl files. Example: 'C:/Games/TOP/shaders/hlsl'.
		vsh_folder: path to the folder using foward slashes and no ending slash where vsh files will be put. Example: 'C:/Games/TOP/shaders/dx9'.
		dx8: if true, converts to supported DX8 vsh files.
--]]

dofile('./hlsl_to_vsh.lua') --loads the hlsl_to_vsh - don't change this

hlsl_to_vsh_folder("../client/shader/dx9_hlsl", "../client/shader/dx8", true)